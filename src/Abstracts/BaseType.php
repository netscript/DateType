<?php
/**
 * Created by PhpStorm.
 * User: WebLinuxGame
 * Date: 2019/6/8
 * Time: 10:17
 */

namespace WebLinuxGame\DateType\Abstracts;

use WebLinuxGame\DateType\Contracts\Type;

/**
 * Class BaseType
 * @package Main\Api
 */
abstract class BaseType implements Type
{
    const TYPE_CODE = 0;

    protected static $type = '';

    protected static $alias = [];

    abstract public static function format($data, $default = null);

    abstract public static function verify($data): bool;

    /**
     * 输出类型名
     */
    public static function type(): string
    {
        if (empty(static::$type) || !is_string(static::$type)) {
            throw new \Exception("no support type");
        }
        return static::$type;
    }

    /**
     * 是否对应类型名
     * @param string $name
     * @return bool
     * @throws \Exception
     */
    public static function isName(string $name): bool
    {
        if (self::type() === $name) {
            return true;
        }
        if (self::isAlias($name)) {
            return true;
        }
        return false;
    }

    /**
     * 是否别名
     * @param string $name
     * @return bool
     */
    public static function isAlias(string $name): bool
    {
        return in_array($name, static::$alias);
    }

    /**
     * 是否对应类型code
     * @param int $code
     * @return bool
     */
    public static function isType(int $code): bool
    {
        return !empty(static::TYPE_CODE) && static::TYPE_CODE === $code;
    }

    /**
     * 获取变量类型
     * @param $data
     * @return string
     */
    public static function getType($data) : string
    {
        return gettype($data);
    }
}