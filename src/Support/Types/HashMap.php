<?php
/**
 * Created by PhpStorm.
 * User: WebLinuxGame
 * Date: 2019/6/8
 * Time: 11:14
 */

namespace WebLinuxGame\DateType\Support\Types;

/**
 * 映射
 * Class HashMap
 * @package Main\Api\DataType
 */
class HashMap extends Arr
{
    const TYPE_CODE =  0x00061;

    protected static $type = 'hashMap';
    protected static $alias = ['map','hashArr'];

    /**
     * @param $data
     * @param Nil $default
     * @return array
     */
    public static function format($data, $default = null)
    {
        if (self::verify($data)) {
            return (array)$data;
        }
        if (is_object($data)) {
            if (method_exists($data, 'toArray')) {
                return self::format($data->toArray());
            }
            return self::format((array)$data);
        }
        if (is_string($data) && !empty($data)) {
            if (self::isStrArr($data)) {
                $data = str_replace(']', '', str_replace('[', '', $data));
                return self::format((array)explode(',', $data));
            }
            if (self::isJsonArr($data)) {
                return self::format((array)json_decode($data, true));
            }
            if (self::isSerializeArr($data)) {
                return self::format((array)unserialize($data));
            }
        }
        return (array)$default;
    }

    /**
     * @param $data
     * @return bool
     */
    public static function verify($data): bool
    {
        if(!is_array($data)){
            return false;
        }
        $count = count(array_filter($data, function ($k) {
            return is_int($k) && $k >= 0;
        }, ARRAY_FILTER_USE_KEY));
        if (count($data) !==$count) {
            return true;
        }
        return false;
    }

    /**
     * @param string $data
     * @return bool
     */
    public static function isSerialize(string $data): bool
    {
         return parent::isSerializeArr($data);
    }

    /**
     * @param string $data
     * @return bool
     */
    public static function isJson(string $data): bool
    {
        return parent::isJsonArr($data);
    }
}