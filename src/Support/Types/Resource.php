<?php
/**
 * Created by PhpStorm.
 * User: WebLinuxGame
 * Date: 2019/6/9
 * Time: 9:46
 */

namespace WebLinuxGame\DateType\Support\Types;


use WebLinuxGame\DateType\Abstracts\BaseType;

/**
 * Class Resource
 * @package WebLinuxGame\DateType\Support\Types
 */
class Resource extends BaseType
{
    const TYPE_CODE = 0x00040;

    protected static $type = 'resource';
    protected static $alias = ['res'];

    /**
     * 格式化
     * @param $data
     * @param null $default
     * @return mixed|null
     */
    public static function format($data, $default = null)
    {
        if (self::verify($data)) {
            return $data;
        }
        return $default;
    }

    /**
     * @param $data
     * @return bool
     */
    public static function verify($data): bool
    {
        if (is_resource($data)) {
            return true;
        }
        return false;
    }

    /**
     * @param $res
     * @return string
     */
    public static function getResourceType($res): string
    {
        if (self::verify($res)) {
            return get_resource_type($res);
        }
        return '';
    }

}