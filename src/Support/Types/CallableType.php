<?php
/**
 * Created by PhpStorm.
 * User: WebLinuxGame
 * Date: 2019/6/9
 * Time: 9:58
 */

namespace WebLinuxGame\DateType\Support\Types;

use WebLinuxGame\DateType\Abstracts\BaseType;

/**
 * 静态调用
 * Class CallableType
 * @package WebLinuxGame\DateType\Support\Types
 */
class CallableType extends BaseType
{
    const TYPE_CODE = 0x00005;
    protected static $type ='callable';
    protected static $alias = ['call'];

    /**
     * @param $data
     * @param null $default
     * @return mixed|null
     */
    public static function format($data, $default = null)
    {
         if(is_callable($data)){
             return $data;
         }
         return $default;
    }

    /**
     * @param $data
     * @return bool
     */
    public static function verify($data): bool
    {
        if(is_callable($data)){
            return true;
        }
        return false;
    }
}