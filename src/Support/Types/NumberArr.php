<?php
/**
 * Created by PhpStorm.
 * User: WebLinuxGame
 * Date: 2019/6/8
 * Time: 11:24
 */

namespace WebLinuxGame\DateType\Support\Types;

use WebLinuxGame\DateType\Abstracts\BaseType;

/**
 * Class NumberArr
 * @package WebLinuxGame\DateType\Support\Types
 */
class NumberArr extends BaseType
{

    /**
     * @param $data
     * @param Nil $default
     * @return array|mixed
     */
    public static function format($data, $default = null)
    {
        if(self::verify($data)){
            return (array)$data;
        }
        $ret = Arr::format($data);
        if(!empty($ret) && self::verify($data)){
            return (array)$ret;
        }
        return (array)$default;
    }

    /**
     * 是否数组
     * @param $data
     * @return bool
     */
    public static function verify($data): bool
    {
        if (!is_array($data)) {
            return false;
        }
        if (count($data) !== array_filter($data, function ($k) {
                return is_int($k) && $k >= 0;
            }, ARRAY_FILTER_USE_KEY)) {
            return false;
        }
        return true;
    }

}