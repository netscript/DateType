<?php
/**
 * Created by PhpStorm.
 * User: WebLinuxGame
 * Date: 2019/6/8
 * Time: 11:10
 */

namespace WebLinuxGame\DateType\Support\Types;

use WebLinuxGame\DateType\Abstracts\BaseType;

/**
 * 数字类型
 * Class Number
 * @package Main\Api\DataType
 */
class Number extends BaseType
{
    const TYPE_CODE = 0x00020;

    protected static $type = 'number';

    protected static $alias = ['int', 'float', 'double',];

    /**
     * 格式化数
     * @param $data
     * @param Nil $default
     * @return float|int|mixed
     */
    public static function format($data, $default = null)
    {
        if (self::verify($data)) {
            if (false === strpos("$data", '.')) {
                return (int)$data;
            }
            $arr = explode('.', "$data");
            if (4 < strlen($arr[1])) {
                return (float)$data;
            }
            return (double)$data;
        }
        if (is_string($data)) {
            if (self::isJsonNumber($data)) {
                return self::format(json_decode($data), $default);
            }
            if (self::isSerializeNumber($data)) {
                return unserialize($data);
            }
        }
        return (int)$default;
    }

    /**
     * 验证
     * @param $data
     * @return bool
     */
    public static function verify($data): bool
    {
        return is_numeric($data);
    }

    /**
     * 是否json 数字
     * @param string $data
     * @return bool
     */
    public static function isJsonNumber(string $data): bool
    {
        if(preg_match('/^"([0-9]{1,}|([0-9]{1,}.[0-9]{1,}))"$/',$data)){
            return true;
        }
        return false;
    }

    /**
     * 是否序列化数字
     * @param string $data
     * @return bool
     */
    public static function isSerializeNumber(string $data): bool
    {
        if(preg_match('/^(i|d):[0-9]{1,};$/',$data)){
            return true;
        }
        return false;
    }

}