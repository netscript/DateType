<?php
/**
 * Created by PhpStorm.
 * User: WebLinuxGame
 * Date: 2019/6/8
 * Time: 15:31
 */

namespace WebLinuxGame\DateType\Support\Types;

use WebLinuxGame\DateType\Abstracts\BaseType;

/**
 * Class Object
 * @package WebLinuxGame\DateType\Support\Types
 */
class Object extends BaseType
{
    const TYPE_CODE =  0x00060;

    protected static $type = 'object';

    protected static $alias = ['object','obj',];

    /**
     * @param $data
     * @param Nil $default
     * @return mixed|object
     */
    public static function format($data, $default = null)
    {
        if (self::verify($data)) {
            return $data;
        }
        if (is_string($data)) {
            if (self::isJsonObject($data)) {
                return json_decode($data);
            }
            if (self::isSerializeObject($data)) {
                return unserialize($data);
            }
        }
        if (is_array($data)) {
            return (object)$data;
        }
        return (object)$default;
    }

    /**
     * @param $data
     * @return bool
     */
    public static function verify($data): bool
    {
        if (is_object($data)) {
            return true;
        }
        return false;
    }

    /**
     * @param $data
     * @return bool
     */
    public static function isJsonObject($data)
    {
        if (preg_match('/^\{.+\}$/', $data)) {
            return true;
        }
        return false;
    }

    /**
     * @param $data
     * @return bool
     */
    public static function isSerializeObject($data)
    {
        if (preg_match('/^O:[0-9]{1,}:.{1,}(:\{.{0,}\})$/', $data)) {
            return true;
        }
        return false;
    }

}