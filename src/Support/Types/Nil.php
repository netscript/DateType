<?php
/**
 * Created by PhpStorm.
 * User: WebLinuxGame
 * Date: 2019/6/9
 * Time: 9:39
 */

namespace WebLinuxGame\DateType\Support\Types;

use WebLinuxGame\DateType\Abstracts\BaseType;

/**
 * 是否Null
 * Class Nil
 * @package WebLinuxGame\DateType\Support\Types
 */
class Nil extends BaseType
{
    const TYPE_CODE = 0x1000;

    protected static $type = 'null';

    protected static $alias = ['Null','nil',];

    /**
     * 格式null
     * @param $data
     * @param null $default
     * @return mixed|null
     */
    public static function format($data, $default = null)
    {
        if (self::verify($data)) {
            return null;
        }
        if (is_string($data)) {
            if (self::isJsonNull($data) || self::isSerializeNull($data)) {
                return null;
            }
        }
        return $data;
    }

    /**
     * @param string $data
     * @return bool
     */
    public static function isJsonNull(string $data): bool
    {
        return 'null' === $data;
    }

    /**
     * @param string $data
     * @return bool
     */
    public static function isSerializeNull(string $data): bool
    {
        return 'N;' === $data;
    }

    public static function verify($data): bool
    {
        if (is_null($data)) {
            return true;
        }
        return false;
    }
}