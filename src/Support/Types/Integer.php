<?php
/**
 * Created by PhpStorm.
 * User: WebLinuxGame
 * Date: 2019/6/8
 * Time: 11:15
 */

namespace WebLinuxGame\DateType\Support\Types;

/**
 * Class Integer
 * @package Main\Api\DataType
 */
class Integer extends Number
{
    const TYPE_CODE = 0x00021;

    protected static $type = 'integer';

    protected static $alias = ['int',];

    /**
     * 整形格式化
     * @param $data
     * @param Nil $default
     * @return int|mixed
     */
    public static function format($data, $default = null)
    {
        if(self::verify($data)){
            return (int)$data;
        }
        if(is_numeric($data)){
            return (int)$data;
        }
        if(is_string($data)){
            if(self::isJsonNumber($data)){
                return (int)json_decode($data);
            }
            if(self::isSerializeNumber($data)){
                return (int)unserialize($data);
            }
        }
        return (int)$default;
    }

    /**
     * 验证
     * @param $data
     * @return bool
     */
    public static function verify($data): bool
    {
        if(!is_numeric($data)){
            return false;
        }
        return is_int($data);
    }

    /**
     * @param string $data
     * @return bool
     */
    public static function isSerializeNumber(string $data): bool
    {
        if(preg_match('/^i:[0-9]{1,};$/',$data)){
            return true;
        }
        return false;
    }

    /**
     * @param string $data
     * @return bool
     */
    public static function isJsonNumber(string $data): bool
    {
        if(preg_match('/^"([0-9]{1,}|([0-9]{1,}.0{1,}))"$/',$data)){
            return true;
        }
        return false;
    }

}