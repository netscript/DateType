<?php
/**
 * Created by PhpStorm.
 * User: WebLinuxGame
 * Date: 2019/6/8
 * Time: 10:20
 */

namespace WebLinuxGame\DateType\Support\Types;

use WebLinuxGame\DateType\Abstracts\BaseType;

/**
 * 字符串类型
 * Class Str
 * @package Main\Api\DataType
 */
class Str extends BaseType
{
    const TYPE_CODE = 0x00001;

    protected static $type = 'string';

    protected static $alias = ['str', 'varchar', 'char', 'word',];

    /**
     * 格式化
     * @param $data
     * @param Nil $default
     * @return string
     */
    public static function format($data, $default = null)
    {
        if (self::verify($data)) {
            if (self::isJsonStr($data)) {
                return self::format((string)json_decode($data));
            }
            if (self::isSerializeStr($data)) {
                return (string)unserialize($data);
            }
            return (string)$data;
        }
        if (is_object($data)) {
            if (method_exists($data, '__toString')) {
                return (string)$data->__toString();
            }
            return (string)serialize($data);
        }
        if (is_array($data)) {
            if (empty($data)) {
                return (string)$default;
            }
            if (NumberArr::verify($data)) {
                return '[' . implode(',', $data) . ']';
            }
            return (string)json_encode($data, JSON_UNESCAPED_UNICODE);
        }
        if (is_null($data)) {
            return (string)$default;
        }
        return "$data";
    }

    /**
     * 验证类型
     * @param $data
     * @return bool
     */
    public static function verify($data): bool
    {
        if (is_string($data)) {
            return true;
        }
        return false;
    }

    /**
     * 是否为json 格式字符串
     * @param string $data
     * @return bool
     */
    public static function isJsonStr(string $data): bool
    {
        if (preg_match('/^".{1,}"$/', $data)) {
            return true;
        }
        return false;
    }

    /**
     * 是否为序列化字符串
     * @param string $data
     * @return bool
     */
    public static function isSerializeStr(string $data): bool
    {
        if (preg_match('/^s:([0-9]{1,}):"(.{0,})";$/', $data)) {
            return true;
        }
        return false;
    }

}