<?php
/**
 * Created by PhpStorm.
 * User: WebLinuxGame
 * Date: 2019/6/8
 * Time: 11:11
 */

namespace WebLinuxGame\DateType\Support\Types;


use WebLinuxGame\DateType\Abstracts\BaseType;

/**
 * 数组类型
 * Class Arr
 * @package Main\Api\DataType
 */
class Arr extends BaseType
{

    const TYPE_CODE = 0x00030;

    protected static $type = 'array';

    protected static $alias = ['arr', 'list', 'map',];

    /**
     * @param $data
     * @param Nil $default
     * @return array
     */
    public static function format($data, $default = null)
    {
        if (self::verify($data)) {
            return (array)$data;
        }
        if (is_object($data)) {
            if (method_exists($data, 'toArray')) {
                return (array)$data->toArray();
            }
            return (array)$data;
        }
        if (is_string($data) && !empty($data)) {
            if (self::isStrArr($data)) {
                $data = str_replace(']', '', str_replace('[', '', $data));
                return (array)explode(',', $data);
            }
            if (self::isJsonArr($data)) {
                return (array)json_decode($data, true);
            }
            if (self::isSerializeArr($data)) {
                return (array)unserialize($data);
            }
        }
        return (array)$default;
    }

    /**
     * @param $data
     * @return bool
     */
    public static function verify($data): bool
    {
        return is_array($data);
    }

    /**
     * 是否为字符串数组
     * @param string $data
     * @return bool
     */
    public static function isStrArr(string $data)
    {
        if (preg_match('/^\[(([0-9]{1,})(,*)){1,}\]$/', $data)) {
            return true;
        }
        return false;
    }

    /**
     * 是否json 数组
     * @param string $data
     * @return bool
     */
    public static function isJsonArr(string $data): bool
    {
        if (preg_match('/^\{(([A-Za-z0-9_"]{1,}:.{1,})(,{0,1})){0,1}\}$/', $data)) {
            return true;
        }
        return false;
    }

    /**
     * 是否序列化数组
     * @param string $data
     * @return bool
     */
    public static function isSerializeArr(string $data): bool
    {
        if (preg_match('/^a:([0-9]{1,}):(\{(.{0,})\})$/', $data)) {
            return true;
        }
        return false;
    }
}