<?php
/**
 * Created by PhpStorm.
 * User: WebLinuxGame
 * Date: 2019/6/8
 * Time: 15:32
 */

namespace WebLinuxGame\DateType\Support\Types;


use WebLinuxGame\DateType\Abstracts\BaseType;

/**
 * Class ListMap
 * @package WebLinuxGame\DateType\Support\Types
 */
class ListMap extends BaseType
{
    const TYPE_CODE = 0x00062;

    protected static $type = 'ListMap';
    protected static $alias = ['List<T>', 'list'];

    /**
     * @param $data
     * @param Nil $default
     * @return array
     */
    public static function format($data, $default = null)
    {
        if (self::verify($data)) {
            return (array)$data;
        }
        if (is_string($data)) {
            if (self::isJsonListMap($data)) {
                return (array)json_decode($data);
            }
            if (self::isSerializeListMap($data)) {
                return (array)unserialize($data);
            }
        }
        return (array)$default;
    }

    /**
     * 验证每个元素是一致
     * @param $data
     * @return bool
     */
    public static function verify($data): bool
    {
        if (!is_array($data)) {
            return false;
        }
        $hash = null;
        foreach ($data as $k => $item) {
            if (!is_int($k) || $k < 0) {
                return false;
            }
            if (is_object($item)) {
                $current = get_class($item);
                if (is_null($hash)) {
                    $hash = $current;
                    continue;
                }
                if ($hash != $current) {
                    return false;
                }
            }
            if (is_array($item)) {
                $keys = array_keys($item);
                sort($keys);
                $current = gettype($item) . '@' . md5(implode(',', $keys));
                if (is_null($hash)) {
                    $hash = $current;
                    continue;
                }
                if ($hash != $current) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * @param string $data
     * @return bool
     */
    public static function isJsonListMap(string $data): bool
    {
        if (!preg_match('/^\[(\[|\{).{0,}(\]|\})\]$/',$data)) {
            return false;
        }
        return self::verify(json_decode($data, true));
    }

    /**
     * @param string $data
     * @return bool
     */
    public static function isSerializeListMap(string $data): bool
    {
        if (!Arr::isSerializeArr($data)) {
            return false;
        }
        return self::verify(unserialize($data));
    }
}