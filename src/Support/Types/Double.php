<?php
/**
 * Created by PhpStorm.
 * User: WebLinuxGame
 * Date: 2019/6/8
 * Time: 15:26
 */

namespace WebLinuxGame\DateType\Support\Types;

/**
 * 双精度数字
 * Class Double
 * @package WebLinuxGame\DateType\Support\Types
 */
class Double extends Number
{
    const TYPE_CODE = 0x00022;

    protected static $type = 'double';
    protected static $alias = ['float','F','double','D'];

    /**
     * 输出
     * @param $data
     * @param Nil $default
     * @return float|int|mixed
     */
    public static function format($data, $default = null)
    {
        return (double)parent::format($data, $default);
    }

    /**
     * @param string $data
     * @return bool
     */
    public static function isSerializeNumber(string $data): bool
    {
        if(preg_match('/^d:[0-9]{1,};$/',$data)){
            return true;
        }
        return false;
    }
}