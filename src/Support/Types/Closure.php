<?php
/**
 * Created by PhpStorm.
 * User: WebLinuxGame
 * Date: 2019/6/9
 * Time: 10:03
 */

namespace WebLinuxGame\DateType\Support\Types;

use Closure as Func;
use WebLinuxGame\DateType\Abstracts\BaseType;

/**
 * 闭包
 * Class Closure
 * @package WebLinuxGame\DateType\Support\Types
 */
class Closure extends BaseType
{
    const TYPE_CODE = 0x00006;

    protected static $type = 'Closure';
    protected static $alias = ['closure','func'];

    /**
     * @param $data
     * @param null $default
     * @return Func|mixed|null
     */
    public static function format($data, $default = null)
    {
        if(self::verify($data)){
            return $data;
        }
        if(CallableType::verify($data)){
            return Func::fromCallable($data);
        }
        return $default;
    }

    /**
     * @param $data
     * @return bool
     */
    public static function verify($data): bool
    {
        if($data instanceof Func){
            return true;
        }
        return false;
    }
}