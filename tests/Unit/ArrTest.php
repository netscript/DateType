<?php
/**
 * Created by PhpStorm.
 * User: WebLinuxGame
 * Date: 2019/6/9
 * Time: 10:13
 */

namespace WebLinuxGame\DateType\Support\Types;

use WebLinuxGame\DateType\Tests\TestCase;

class ArrTest extends TestCase
{

    public function testIsStrArr()
    {
        $data = range(0, 100);
        $data = json_encode($data);
        $this->assertTrue(Arr::isStrArr($data), '类型异常');
    }

    public function testIsJsonArr()
    {
        $data = [
            'name' => 'test',
            1,
            'sex' => 1,
            'age' => 18,
            'books' => ['php', 'java', 'golang', 'vue', 'python'],
        ];
        $data = json_encode($data);

        $this->assertTrue(Arr::isJsonArr($data), '类型异常');
    }

    public function testFormat()
    {
        $data = [
            'name' => 'test',
            1,
            'sex' => 1,
            'age' => 18,
            'books' => ['php', 'java', 'golang', 'vue', 'python'],
        ];
        $this->assertTrue($data == Arr::format($data), '格式化异常');
        $json = json_encode($data);

        $this->assertTrue($data == Arr::format($json), '格式化异常');
        $ser = serialize($data);

        $this->assertTrue($data == Arr::format($ser), '格式化异常');
    }

    public function testVerify()
    {
        $this->assertTrue(Arr::verify(array()), '类型异常');
    }

    public function testIsSerializeArr()
    {
        $data = [
            'name' => 'test',
            1,
            'sex' => 1,
            'age' => 18,
            'books' => ['php', 'java', 'golang', 'vue', 'python'],
        ];

        $ser = serialize($data);

        $this->assertTrue(Arr::isSerializeArr($ser), '格式化异常');
    }
}
