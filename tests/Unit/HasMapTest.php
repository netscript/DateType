<?php
/**
 * Created by PhpStorm.
 * User: WebLinuxGame
 * Date: 2019/6/9
 * Time: 10:25
 */

namespace WebLinuxGame\DateType\Support\Types;


use WebLinuxGame\DateType\Tests\TestCase;

class HasMapTest extends TestCase
{

    public function testVerify()
    {
        $data = ['name'=>'hashMap',0,'sex'=>1];
        $this->assertTrue(false ==HashMap::verify(serialize($data)),'类型持久化检查异常');
        $this->assertTrue(false == HashMap::verify(json_encode($data)),'类型json化检查异常');
        $this->assertTrue(HashMap::verify($data),'类型检查异常');
        $this->assertTrue(false == HashMap::verify(range(0,100)),'类型持检查异常');
    }

    public function testIsJson()
    {
        $data = ['name'=>'hashMap',0,'sex'=>1];
        $this->assertTrue(HashMap::isJson(json_encode($data)),'类型json化检查异常');
    }

    public function testFormat()
    {
        $data = ['name'=>'hashMap',0,'sex'=>1];
        $this->assertSame($data ,HashMap::format(serialize($data)),'类型格式化异常');
        $this->assertSame($data,HashMap::format(json_encode($data)),'类型格式化异常');
        $this->assertSame($data,HashMap::format($data),'类型格式化异常');
        $this->assertSame([],HashMap::format(range(0,100)),'类型格式化异常');
    }

    public function testIsSerialize()
    {
        $data = ['name'=>'hashMap',0,'sex'=>1];
        $this->assertTrue(HashMap::isSerialize(serialize($data)),'类型serialize化检查异常');
    }
}
