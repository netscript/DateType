<?php
/**
 * Created by PhpStorm.
 * User: WebLinuxGame
 * Date: 2019/6/9
 * Time: 10:42
 */

namespace WebLinuxGame\DateType\Support\Types;


use WebLinuxGame\DateType\Tests\TestCase;

class ListMapTest extends TestCase
{

    public function getItem(bool $strict = false)
    {
        $object = (object)null;
        $object->name = time() + rand(0,1000);
        $object->sex = rand(0, 1);
        if (rand(0, 1) && empty($strict)) {
            $object->books = ['python', 'gloang', 'php', 'java', 'javascript', 'clang'];
        }
        return $object;
    }

    public function getListMap(bool $strict = false)
    {
        return [
            $this->getItem($strict),
            $this->getItem($strict),
            $this->getItem($strict),
            $this->getItem($strict),
            $this->getItem($strict),
            $this->getItem($strict),
            $this->getItem($strict),
            $this->getItem($strict),
            $this->getItem($strict),
            $this->getItem($strict),
            $this->getItem($strict),
            $this->getItem($strict),
            $this->getItem($strict),
            $this->getItem($strict),
            $this->getItem($strict),
            $this->getItem($strict),
            $this->getItem($strict),
            $this->getItem($strict),
            $this->getItem($strict),
            $this->getItem($strict),
        ];
    }

    public function getListMapBad()
    {
        return [
            $this->getItem(),
            $this->getItem(),
            $this->getItem(),
            $this->getItem(),
            new HashMap(),
        ];
    }

    public function getListMapBad2()
    {
        return [
            (array)$this->getItem(),
            (array)$this->getItem(),
            (array)$this->getItem(),
            (array)$this->getItem(),
            (array)$this->getItem(),
        ];
    }

    public function testIsSerializeListMap()
    {
        $this->assertTrue(ListMap::isSerializeListMap(serialize($this->getListMap())), '类型检查异常');
        $this->assertTrue(true !== ListMap::isSerializeListMap(serialize($this->getListMapBad())), '类型检查异常');
        $this->assertTrue(true !== ListMap::isSerializeListMap(serialize($this->getListMapBad2())), '类型检查异常');
    }

    public function testVerify()
    {
        $this->assertTrue(false == ListMap::verify(serialize($this->getListMap())), '类型检查异常');
        $this->assertTrue(false == ListMap::verify(json_encode($this->getListMap())), '类型检查异常');
        $this->assertTrue(false == ListMap::verify(serialize($this->getListMapBad2())), '类型检查异常');
        $this->assertTrue(false == ListMap::verify($this->getListMapBad2()), '类型检查异常');
        $this->assertTrue(false == ListMap::verify($this->getListMapBad()), '类型检查异常');
        $this->assertTrue(ListMap::verify($this->getListMap()), '类型检查异常');
    }

    public function testIsJsonListMap()
    {
        $this->assertTrue(ListMap::isJsonListMap(json_encode($this->getListMap(true))), '类型检查异常');
        $this->assertTrue(false == ListMap::isJsonListMap(json_encode($this->getListMap())), '类型检查异常');
    }

    public function testFormat()
    {
        $data = $this->getListMap(true);
        $data2 = $this->getListMap();
        $this->assertSame($data,ListMap::format($data), '格式化异常');
        $this->assertTrue($data == ListMap::format(serialize($data)), '格式化异常');
        $this->assertTrue($data == ListMap::format(json_encode($data)), '格式化异常');
        $this->assertTrue($data2 == ListMap::format(serialize($data2)), '格式化异常');
        $this->assertSame([], ListMap::format(json_encode($data2)), '格式化异常');
    }
}
