<?php
/**
 * Created by PhpStorm.
 * User: WebLinuxGame
 * Date: 2019/6/8
 * Time: 13:21
 */

namespace WebLinuxGame\DateType\Support\Types;

use WebLinuxGame\DateType\Tests\TestCase;

/**
 * Class StrTest
 * @package WebLinuxGame\DateType\Support\Types
 */
class StrTest extends TestCase
{

    public function testVerify()
    {
        $data = "sdf";
        $this->assertTrue(Str::verify($data),'数据类型异常');
    }

    public function testIsJsonStr()
    {
        $data ='"name"';
        $this->assertTrue(Str::verify($data),'数据类型异常');
    }

    public function testFormat()
    {
        $data = [];
        $this->assertTrue("" === Str::format($data),'格式化异常');
    }
}
