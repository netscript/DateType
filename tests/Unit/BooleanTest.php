<?php
/**
 * Created by PhpStorm.
 * User: WebLinuxGame
 * Date: 2019/6/9
 * Time: 10:14
 */

namespace WebLinuxGame\DateType\Support\Types;

use WebLinuxGame\DateType\Tests\TestCase;

/**
 * Class BooleanTest
 * @package WebLinuxGame\DateType\Support\Types
 */
class BooleanTest extends TestCase
{

    public function testVerify()
    {
        $this->assertTrue(Boolean::verify(false),'类型检查异常');
    }

    public function testIsSerializeBool()
    {
        $this->assertTrue(Boolean::isSerializeBool(serialize(true)),'类型持久化检查异常');
    }

    public function testFormat()
    {
        $this->assertTrue(true == Boolean::format(serialize(true)),'类型格式化异常');
        $this->assertTrue(true == Boolean::format(json_encode(true)),'类型格式化异常');
        $this->assertTrue(true == Boolean::format(true),'类型格式化异常');
        $this->assertTrue(true == Boolean::format(1),'类型格式化异常');
        $this->assertTrue(true == Boolean::format((object)null),'类型格式化异常');
    }

    public function testIsJsonBool()
    {
        $this->assertTrue(Boolean::isJsonBool(json_encode(true)),'类型json化检查异常');
    }
}
