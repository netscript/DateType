<?php
/**
 * Created by PhpStorm.
 * User: WebLinuxGame
 * Date: 2019/6/9
 * Time: 11:47
 */

namespace WebLinuxGame\DateType\Support\Types;


use WebLinuxGame\DateType\Contracts\Type;
use WebLinuxGame\DateType\Tests\TestCase;
use WebLinuxGame\DateType\Abstracts\BaseType;
use WebLinuxGame\DateType\Traits\LoggerTrait;
use WebLinuxGame\DateType\Traits\ReflectionTrait;

/**
 * Class AnyTest
 * @package WebLinuxGame\DateType\Support\Types
 */
class AnyTest extends TestCase
{

    public function testIsInstanceOf()
    {
        $ret = Any::isInstance(Type::class,new Arr());
        $this->assertTrue($ret,'类型实例化检查失败');
        $ret = Any::isInstance(Type::class,new Number());
        $this->assertTrue($ret,'类型实例化检查失败');
        $ret = Any::isInstance(BaseType::class,new Number());
        $this->assertTrue($ret,'类型实例化检查失败');
    }

    public function testHasTraits()
    {
        $ret = Any::hasTraits(Any::class,[ReflectionTrait::class,LoggerTrait::class]);
        $this->assertNotEmpty($ret,'检查功能块失败');
        $this->assertTrue(true == $ret[ReflectionTrait::class],'常量检查失败');
        $this->assertTrue(false == $ret[LoggerTrait::class],'常量检查失败');
    }

    public function testHasConst()
    {
        $ret = Any::hasConst(Number::class,['TYPE_CODE']);
        $this->assertTrue($ret['TYPE_CODE'],'常量检查失败');
        $ret = Any::hasConst(new Number(),['TYPE_CODE']);
        $this->assertTrue($ret['TYPE_CODE'],'常量检查失败');
    }

    public function testHasInterface()
    {
        $ret = Any::hasInterface(Number::class,[Type::class]);
        $this->assertTrue($ret[Type::class],'接口检查失败');
        $ret = Any::hasInterface(new Number(),[Type::class]);
        $this->assertTrue($ret[Type::class],'接口检查失败');

    }

    public function testIsInstance()
    {
        $this->assertNotEmpty(Any::isInstance(TestCase::class,$this),'实例检查失败');
        $this->assertNotEmpty(false == Any::isInstance(TestCase::class,(object)null),'实例检查失败');
    }

    public function testGetReflection()
    {
        $this->assertNotEmpty(Any::getReflection($this),'反射类获取异常');
    }

    public function testWrite()
    {
        $this->assertNotEmpty(Any::write('测试日志',get_class_vars(get_class($this))),'测试写日志失败');
    }

    public  function testHasMethod()
    {
        $ref = Any::getReflection(Any::class);
        $this->assertTrue($ref->hasMethod('write'),'方法检查失败');
        $this->assertNotEmpty($ref,'获取反射失败');
    }
}
